# Overview

Simple mesh type protocol to discover others nodes in subnet.

Next we would like to have /etc/hosts common or share common list of objects.

* hosts boot up in some network,
* cannot use any external service,
* cannot broadcast,
* there is no infrastructure yet (so no etcd/consul)
* they need to bootstrap with zero knowledge,
* decentralization.

## Sell approach: discovery.sh
Run script on each hosts.
Each of the hosts listen on specific port (the same across all the hosts).
Next each of them periodically scan subnet over TCP/IP to find others. 
Next each of hosts provision /etc/hosts with others. 

## Python approach: himate.py
Run himate.py on each hosts.
Each of them listen on specific port and scan network in search for others. 
Next:
* they can do some hello/hello to make sure, it is our fellow. 
* they can share common list of objects,
* this list comes from TCP discovery or from other hosts. All sources are merged together,
* hello/hello is also a healthcheck. So each host can monitor each other an update a list to kick out dead ones. 
* each host can request a list from other to compare/merge.




