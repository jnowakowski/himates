#!/usr/bin/env python
import sys
import threading
import socket
import select
import time

def cleanup():
    sys.exit()

mates = []

def get_port():
    if len(sys.argv) > 1:
        try:
            return int(sys.argv[1])
        except:
            pass
    return 12345

def server(port):
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(("0.0.0.0", port))
    server_socket.listen(10)
    print("listening on 0.0.0.0:%s " % str(port))
    while 1:
            connection, client_address = server_socket.accept()
            print("Client (%s, %s) connected" % client_address)
            connection.close()
    server_socket.close()

def client(host, port):
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.settimeout(2)
    client_socket.connect((host,port))
    client_socket.close()

if __name__ == "__main__":
    try:
        port = get_port()
        server_thread = threading.Thread(target=server, args=(port,))
        server_thread.start()

        while 1:
            client("127.0.0.1", port)
            client("172.18.0.15", port)
            time.sleep(2)
            pass
    except:
        cleanup()

