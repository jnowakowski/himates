#!/bin/bash

# install nmap
sudo apt-get update
sudo apt-get install -y nmap

# port for discovery
PORT=12345

# clean exit after CTRL+C
function cleanup() {
	kill -9 ${NCPID}
	exit 
}
trap cleanup INT

# listen on port in background, get pid to kill on exit
nc -l -p ${PORT} &
NCPID=$!

#get network for scanning
NETWORK=$(ip -o -f inet addr show eth0| awk '{print $4}')

#scan network for others, update hosts, sleep random, repeat
while true; do
	echo "127.0.0.1 localhost" > hosts
	# iterate over nmap scan results, at least myself should be found (fair enough)
	for IP in $(sudo nmap -n -p ${PORT} ${NETWORK} -oG -| grep open| awk '{print $2}'); do 
		DNS="ip-${IP//./-}"
		ALIAS="myhostnames${IP##*.}"
		echo "${IP} ${DNS} ${ALIAS}" >> hosts
	done
	sudo mv hosts /etc/hosts
	sleep $(( (RANDOM % 60) + 60 ))
done


